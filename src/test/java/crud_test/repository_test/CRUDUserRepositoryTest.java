package crud_test.repository_test;

import com.iteco.linealex.api.repository.IUserRepository;
import com.iteco.linealex.config.HibernateConfig;
import com.iteco.linealex.model.User;
import crud_test.AbstractTest;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.*;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {HibernateConfig.class,})
public class CRUDUserRepositoryTest extends AbstractTest {

    @Autowired
    @Qualifier("user_repository")
    IUserRepository userRepository;

    @Nullable
    User user;

    @Before
    public void initEnvironment() {
        user = new User("test", "11111111");
        userRepository.save(user);
    }

    @After
    public void cleanUp() {
        userRepository.delete(user);
    }

    @Test
    public void findByLoginPositiveTest() {
        Assert.assertNotNull(userRepository.findOneByLogin(user.getLogin()));
        Assert.assertTrue(userRepository.findOneByLogin(user.getLogin()).getLogin().equals(user.getLogin()));
    }

    @Test
    public void findByLoginNegativeTest() {
        Assert.assertNull(userRepository.findOneByLogin("test_"));
    }

    @Test
    public void findByIdPositiveTest() {
        Assert.assertNotNull(userRepository.findById(user.getId()));
        Assert.assertTrue(userRepository.findById(user.getId()).get().getLogin().equals(user.getLogin()));
    }

    @Test
    public void findByIdNegativeTest() {
        Assert.assertSame(Optional.empty(), userRepository.findById(UUID.randomUUID().toString()));
    }

    @Test
    public void findAllPositiveTest() {
        Assert.assertNotNull(userRepository.findAll());
        List<User> users = (ArrayList<User>) userRepository.findAll();
        Assert.assertTrue(users.stream().filter(
                u -> u.getId().equals(user.getId()))
                .findFirst().get().getId().equals(user.getId()));
    }

    @Test(expected = NoSuchElementException.class)
    public void findAllNegativeTest() {
        Assert.assertNotNull(userRepository.findAll());
        List<User> users = (ArrayList<User>) userRepository.findAll();
        Assert.assertFalse(users.stream().filter(
                u -> u.getId().equals(new User().getId()))
                .findFirst().get().getId().equals(new User().getId()));
    }

    @Test
    public void createPositiveTest() {
        Assert.assertNotNull(userRepository.findById(user.getId()));
        Assert.assertTrue(userRepository.findById(user.getId()).get().getId().equals(user.getId()));
    }

    @Test
    public void createNegativeTest() {
        Assert.assertSame(Optional.empty(), userRepository.findById(UUID.randomUUID().toString()));
    }

    @Test
    public void savePositiveTest() {
        Assert.assertNotNull(userRepository.findById(user.getId()));
        Assert.assertTrue(userRepository.findById(user.getId()).get().getId().equals(user.getId()));
        user.setLogin("ahalay-mahalay");
        userRepository.save(user);
        Assert.assertNotNull(userRepository.findById(user.getId()));
        Assert.assertTrue(userRepository.findById(user.getId()).get().getLogin().equals("ahalay-mahalay"));
    }

    @Test
    public void saveNegativeTest() {
        Assert.assertNotNull(userRepository.findById(user.getId()));
        Assert.assertTrue(userRepository.findById(user.getId()).get().getId().equals(user.getId()));
        user.setLogin("ahalay-mahalay");
        userRepository.save(user);
        Assert.assertNotNull(userRepository.findById(user.getId()));
        Assert.assertFalse(userRepository.findById(user.getId()).get().getLogin().equals("test"));
    }

}