package crud_test.repository_test;

import com.iteco.linealex.api.repository.IProjectRepository;
import com.iteco.linealex.api.repository.IUserRepository;
import com.iteco.linealex.enumerate.Status;
import com.iteco.linealex.model.Project;
import com.iteco.linealex.model.User;
import crud_test.AbstractTest;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.UUID;

public class CRUDProjectRepositoryTest extends AbstractTest {

    @Autowired
    @Qualifier("user_repository")
    IUserRepository userRepository;

    @Autowired
    @Qualifier("project_repository")
    IProjectRepository projectRepository;

    @Nullable
    User user;

    @Nullable
    Project project;

    @Before
    public void initEnvironment() {
        user = new User("test", "11111111");
        userRepository.save(user);
        project = new Project();
        project.setUser(user);
        user.getProjects().add(project);
        project.setName("test");
        project.setDescription("test description");
        project.setStatus(Status.PLANNED);
        projectRepository.save(project);
    }

    @After
    public void cleanUp() {
        projectRepository.delete(project);
        userRepository.delete(user);
    }

    @Test
    public void findByNamePositiveTest() {
        Assert.assertNotNull(projectRepository.findOneByName(project.getName()));
        Assert.assertTrue(projectRepository.findOneByName(project.getName()).getId().equals(project.getId()));
    }

    @Test
    public void findByNameNegativeTest() {
        Assert.assertNull(projectRepository.findOneByName("project test"));
    }

    @Test
    public void findByIdPositiveTest() {
        Assert.assertNotNull(projectRepository.findById(project.getId()));
        Assert.assertTrue(projectRepository.findById(project.getId()).get().getId().equals(project.getId()));
    }

    @Test
    public void findByIdNegativeTest() {
        Assert.assertSame(Optional.empty(), projectRepository.findById(UUID.randomUUID().toString()));
    }

    @Test
    public void findAllPositiveTest() {
        Assert.assertNotNull(projectRepository.findAll());
        List<Project> projects = (List<Project>) projectRepository.findAll();
        Assert.assertTrue(projects.size() > 0);
        Assert.assertTrue(projects.stream().filter(
                p -> p.getId().equals(project.getId()))
                .findFirst().get().getId().equals(project.getId()));
    }

    @Test(expected = NoSuchElementException.class)
    public void findAllNegativeTest() {
        Assert.assertNotNull(projectRepository.findAll());
        List<Project> projects = (List<Project>) projectRepository.findAll();
        Assert.assertTrue(projects.size() > 0);
        Project testProject = new Project();
        Assert.assertFalse(projects.stream().filter(
                p -> p.getId().equals(testProject.getId()))
                .findFirst().get().getId().equals(testProject.getId()));
    }

    @Test
    public void createPositiveTest() {
        Assert.assertNotNull(projectRepository.findById(project.getId()));
        Assert.assertTrue(projectRepository.findById(project.getId()).get().getId().equals(project.getId()));
    }

    @Test
    public void createNegativeTest() {
        Assert.assertSame(Optional.empty(), projectRepository.findById(UUID.randomUUID().toString()));
    }

    @Test
    public void savePositiveTest() {
        Assert.assertNotNull(projectRepository.findById(project.getId()));
        Assert.assertTrue(projectRepository.findById(project.getId()).get().getId().equals(project.getId()));
        project.setStatus(Status.DONE);
        projectRepository.save(project);
        Assert.assertNotNull(projectRepository.findById(project.getId()));
        Assert.assertTrue(projectRepository.findById(project.getId()).get().getStatus().equals(project.getStatus()));
    }

    @Test
    public void saveNegativeTest() {
        Assert.assertNotNull(projectRepository.findById(project.getId()));
        Assert.assertTrue(projectRepository.findById(project.getId()).get().getId().equals(project.getId()));
        project.setStatus(Status.DONE);
        projectRepository.save(project);
        Assert.assertNotNull(projectRepository.findById(project.getId()));
        Assert.assertFalse(projectRepository.findById(project.getId()).get().getStatus().equals(Status.PLANNED));
    }


}