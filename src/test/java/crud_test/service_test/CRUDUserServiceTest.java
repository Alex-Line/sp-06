package crud_test.service_test;

import com.iteco.linealex.api.service.IUserService;
import com.iteco.linealex.config.WebMvcConfig;
import com.iteco.linealex.model.User;
import crud_test.AbstractTest;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.UUID;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {WebMvcConfig.class})
public class CRUDUserServiceTest extends AbstractTest {

    @Autowired
    @Qualifier("userService")
    IUserService userService;

    @Nullable
    User user;

    @Before
    public void initEnvironment() {
        user = new User("test", "11111111");
        userService.persist(user);
    }

    @After
    public void cleanUp() {
        userService.removeEntity(user.getId());
    }

    @Test
    public void findByLoginPositiveTest() {
        Assert.assertNotNull(userService.getUser(user.getLogin()));
        Assert.assertTrue(userService.getUser(user.getLogin()).getLogin().equals(user.getLogin()));
    }

    @Test
    public void findByLoginNegativeTest() {
        Assert.assertNull(userService.getUser("test_"));
    }

    @Test
    public void findByIdPositiveTest() {
        Assert.assertNotNull(userService.getEntityById(user.getId()));
        Assert.assertTrue(userService.getEntityById(user.getId()).getId().equals(user.getId()));
    }

    @Test(expected = NoSuchElementException.class)
    public void findByIdNegativeTest() {
        User test = userService.getEntityById(UUID.randomUUID().toString());
    }

    @Test
    public void findAllPositiveTest() {
        Assert.assertFalse(userService.getAllEntities().isEmpty());
        List<User> users = (ArrayList<User>) userService.getAllEntities();
        Assert.assertTrue(users.stream().filter(
                u -> u.getId().equals(user.getId()))
                .findFirst().get().getId().equals(user.getId()));
    }

    @Test(expected = NoSuchElementException.class)
    public void findAllNegativeTest() {
        Assert.assertFalse(userService.getAllEntities().isEmpty());
        List<User> users = (ArrayList<User>) userService.getAllEntities();
        Assert.assertFalse(users.stream().filter(
                u -> u.getId().equals(new User().getId()))
                .findFirst().get().getId().equals(new User().getId()));
    }

    @Test
    public void createPositiveTest() {
        Assert.assertNotNull(userService.getEntityById(user.getId()));
        Assert.assertTrue(userService.getEntityById(user.getId()).getId().equals(user.getId()));
    }

    @Test(expected = NoSuchElementException.class)
    public void createNegativeTest() {
        User test = userService.getEntityById(UUID.randomUUID().toString());
    }

    @Test
    public void mergePositiveTest() {
        Assert.assertNotNull(userService.getEntityById(user.getId()));
        Assert.assertTrue(userService.getEntityById(user.getId()).getId().equals(user.getId()));
        user.setLogin("olo-lo");
        userService.merge(user);
        Assert.assertNotNull(userService.getEntityById(user.getId()));
        Assert.assertTrue(userService.getEntityById(user.getId()).getLogin().equals(user.getLogin()));
    }

    @Test
    public void mergeNegativeTest() {
        Assert.assertNotNull(userService.getEntityById(user.getId()));
        Assert.assertTrue(userService.getEntityById(user.getId()).getId().equals(user.getId()));
        user.setLogin("olo-lo");
        userService.merge(user);
        Assert.assertNotNull(userService.getEntityById(user.getId()));
        Assert.assertFalse(userService.getEntityById(user.getId()).getLogin().equals("test"));
    }

}