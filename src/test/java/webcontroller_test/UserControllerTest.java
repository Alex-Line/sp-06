package webcontroller_test;

import com.iteco.linealex.api.repository.IUserRepository;
import com.iteco.linealex.config.HibernateConfig;
import com.iteco.linealex.config.WebMvcConfig;
import com.iteco.linealex.model.User;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.NoSuchElementException;

@WebAppConfiguration
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {WebMvcConfig.class, HibernateConfig.class})
public class UserControllerTest {

    private MockMvc mockMvc;

    @Autowired
    WebApplicationContext context;

    @Autowired
    IUserRepository userRepository;

    @Nullable
    User user;

    @Before
    public void initEnvironment() {
        mockMvc = MockMvcBuilders.webAppContextSetup(context).build();
        user = new User("test", "11111111");
        userRepository.save(user);
    }

    @After
    public void cleanUp() {
        SecurityContextHolder.clearContext();
        userRepository.delete(user);
    }

    @Test
    public void getHomePageTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("index"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/index.jsp"));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void getUserListTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/list"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("userlist"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/userlist.jsp"));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void viewUserTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/view")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("userview"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/userview.jsp"));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void editUserTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/edit")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("useredit"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/useredit.jsp"));
    }

    @Test(expected = NoSuchElementException.class)
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void deleteUserTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/delete")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().is(302));
        User test = userRepository.findById(user.getId()).get();
        Assert.assertNull(test);
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void createUserTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/create")
                .param("login", "login")
                .param("password", "password"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.view().name("usercreate"))
                .andExpect(MockMvcResultMatchers.forwardedUrl("/WEB-INF/views/usercreate.jsp"));
    }

    @Test
    @WithMockUser(username = "admin", roles = "ADMINISTRATOR")
    public void selectUserTest() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/user/select")
                .param("userId", user.getId()))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

}