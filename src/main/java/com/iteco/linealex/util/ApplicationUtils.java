package com.iteco.linealex.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public final class ApplicationUtils {

    /**
     * Utils for work with dates
     */
    @NotNull
    private final static SimpleDateFormat FORMAT = new SimpleDateFormat("dd.MM.yyyy");

    @NotNull
    public static String formatDateToString(@Nullable final Date date) {
        if (date == null) return FORMAT.format(new Date());
        return FORMAT.format(date);
    }

    public static Date formatStringToDate(@Nullable final String stringDate) throws Exception {
        try {
            return FORMAT.parse(stringDate);
        } catch (ParseException e) {
            throw new Exception("Wrong date format");
        }
    }

}