package com.iteco.linealex.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.iteco.linealex.api.service.IProjectService;
import com.iteco.linealex.api.service.IUserService;
import com.iteco.linealex.enumerate.Status;
import com.iteco.linealex.model.Task;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
@Component
@NoArgsConstructor
public class TaskDto {

    static IProjectService staticProjectService;

    static IUserService staticUserService;

    @Autowired
    IProjectService projectService;

    @Autowired
    IUserService userService;

    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    String name;

    @Nullable
    String description;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DD'T'hh:mm:ss±hh:mm")
    Date dateStart;

    @Nullable
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DD'T'hh:mm:ss±hh:mm")
    Date dateFinish;

    @NotNull
    String userId;

    @NotNull
    Status status;

    @Nullable
    private String projectId;

    @PostConstruct
    void init() {
        staticProjectService = projectService;
        staticUserService = userService;
    }

    @NotNull
    public static Task toTask(@NotNull final TaskDto taskDto) throws Exception {
        @NotNull final Task task = new Task();
        task.setId(taskDto.getId());
        task.setUser(staticUserService.getEntityById(taskDto.getUserId()));
        task.setProject(staticProjectService.getEntityById(taskDto.getProjectId()));
        task.setName(taskDto.getName());
        task.setDescription(taskDto.getDescription());
        task.setStatus(taskDto.getStatus());
        task.setDateStart(taskDto.getDateStart());
        task.setDateFinish(taskDto.getDateFinish());
        return task;
    }

}