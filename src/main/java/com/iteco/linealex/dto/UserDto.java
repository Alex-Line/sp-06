package com.iteco.linealex.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.iteco.linealex.api.service.IProjectService;
import com.iteco.linealex.api.service.IRoleService;
import com.iteco.linealex.api.service.ITaskService;
import com.iteco.linealex.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

@Getter
@Setter
@Component
@NoArgsConstructor
public class UserDto {

    @Nullable
    static IProjectService staticProjectService;

    @Nullable
    static ITaskService staticTaskService;

    @Nullable
    static IRoleService staticRoleService;

    @Autowired
    @JsonIgnore
    IProjectService projectService;

    @Autowired
    @JsonIgnore
    ITaskService taskService;

    @Autowired
    @JsonIgnore
    IRoleService roleService;

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String login = "unnamed";

    @Nullable
    private String hashPassword;

    @NotNull
    private Collection<String> roles = new ArrayList<>();

    @PostConstruct
    void init() {
        staticProjectService = projectService;
        staticTaskService = taskService;
        staticRoleService = roleService;
    }

    @NotNull
    public static User toUser(@NotNull final UserDto userDto) throws Exception {
        @NotNull final User user = new User();
        user.setId(userDto.getId());
        user.setLogin(userDto.getLogin());
        user.setHashPassword(userDto.getHashPassword());
        user.setProjects(new ArrayList<>(staticProjectService.getAllEntities(userDto.getId())));
        user.setTasks(new ArrayList<>(staticTaskService.getAllEntities(userDto.getId())));
        user.setRoles(new ArrayList<>(staticRoleService.getAllEntitiesByUserId(userDto.getId())));
        return user;
    }

}