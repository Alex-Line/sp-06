package com.iteco.linealex.service;

import com.iteco.linealex.api.repository.IRoleRepository;
import com.iteco.linealex.api.service.IRoleService;
import com.iteco.linealex.enumerate.RoleType;
import com.iteco.linealex.model.Role;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@Transactional
@Service("roleService")
public class RoleService implements IRoleService {

    @Autowired
    IRoleRepository roleRepository;

    @Override
    @Transactional
    public void persist(@Nullable final Role entity) {
        if (entity == null || entity.getId().isEmpty()) return;
        roleRepository.save(entity);
    }

    @Override
    @Transactional
    public void persist(@NotNull final Collection<Role> collection) {
        if (collection.isEmpty()) return;
        for (Role role : collection) {
            if (role == null) continue;
            roleRepository.save(role);
        }
    }

    @Override
    @Transactional
    public void merge(@Nullable final Role entity) {
        if (entity == null || entity.getId().isEmpty()) return;
        roleRepository.save(entity);
    }

    @Nullable
    @Override
    public Role getEntityById(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return null;
        return roleRepository.findById(entityId).get();
    }

    @Override
    public @NotNull Collection<Role> getAllEntities() {
        return (Collection<Role>) roleRepository.findAll();
    }

    @NotNull
    public Collection<Role> getAllEntitiesById(@NotNull final Collection<String> collectionId) {
        if (collectionId.isEmpty()) return Collections.EMPTY_LIST;
        Collection<Role> roles = new ArrayList<>();
        for (String id : collectionId) {
            Role role = getEntityById(id);
            if (role != null) roles.add(role);
        }
        return roles;
    }

    @NotNull
    @Override
    public Collection<Role> getAllEntitiesByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        return roleRepository.findAllByUserId(userId);
    }

    @NotNull
    @Override
    public Role getRoleByType(@Nullable final RoleType roleType) {
        if (roleType == null) return roleRepository.findByRoleType(RoleType.ORDINARY_USER);
        return roleRepository.findByRoleType(roleType);
    }

    @Override
    @Transactional
    public void removeEntity(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return;
        roleRepository.deleteById(entityId);
    }

    @Override
    @Transactional
    public void removeAllEntities() {
        roleRepository.deleteAll();
    }

}