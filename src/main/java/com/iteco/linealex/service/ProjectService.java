package com.iteco.linealex.service;

import com.iteco.linealex.api.repository.IProjectRepository;
import com.iteco.linealex.api.service.IProjectService;
import com.iteco.linealex.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Collections;

@Transactional
@Service("projectService")
public final class ProjectService extends AbstractTMService<Project> implements IProjectService {

    @Autowired
    private IProjectRepository projectRepository;

    @PostConstruct
    void init() {
        projectRepository.deleteAll();
    }

    @Nullable
    @Override
    public Project getEntityById(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return null;
        return projectRepository.findById(entityId).get();
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntities() {
        return (Collection<Project>) projectRepository.findAll();
    }

    @Override
    @Transactional
    public void persist(@Nullable final Project project) {
        if (project == null) return;
        if (project.getName() == null || project.getName().isEmpty()) return;
        if (project.getDescription() == null || project.getDescription().isEmpty()) return;
        projectRepository.save(project);
    }

    @Override
    public void persist(@NotNull Collection<Project> collection) {
        if (collection.isEmpty()) return;
        for (@Nullable final Project project : collection) {
            if (project == null) continue;
            persist(project);
        }
    }

    @Override
    @Transactional
    public void removeEntity(@Nullable final String entityId) {
        if (entityId == null || entityId.isEmpty()) return;
        projectRepository.deleteById(entityId);
    }

    @Override
    @Transactional
    public void removeAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return;
        projectRepository.deleteAllByUserId(userId);
    }

    @Override
    @Transactional
    public void removeAllEntities() {
        projectRepository.deleteAll();
    }

    @Override
    @Transactional
    public void merge(@Nullable final Project entity) {
        if (entity == null) return;
        if (entity.getName() == null || entity.getName().isEmpty()) return;
        projectRepository.save(entity);
    }

    @Nullable
    @Override
    public Project getEntityByName(@Nullable final String entityName) {
        if (entityName == null || entityName.isEmpty()) return null;
        @Nullable final Project project = projectRepository.findOneByName(entityName);
        return project;
    }

    @Nullable
    @Override
    public Project getEntityByName(
            @Nullable final String userId,
            @Nullable final String entityName
    ) {
        if (userId == null || userId.isEmpty()) return null;
        if (entityName == null || entityName.isEmpty()) return null;
        @Nullable final Project project = projectRepository.findOneByNameAndUserId(userId, entityName);
        return project;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesByName(
            @Nullable final String userId,
            @Nullable final String pattern
    ) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Collection<Project> collection = projectRepository.findAllByNameAndUserId(userId, pattern);
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesByName(@Nullable final String pattern) {
        if (pattern == null || pattern.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Collection<Project> collection = projectRepository.findAllByNameLike(pattern);
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStartDate() {
        @NotNull final Collection<Project> collection = projectRepository.findAllSortedByDateStart();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStartDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Collection<Project> collection = projectRepository.findAllByUserIdSortedByDateStart(userId);
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByFinishDate() {
        @NotNull final Collection<Project> collection = projectRepository.findAllSortedByDateFinish();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByFinishDate(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Collection<Project> collection = projectRepository.findAllByUserIdSortedByDateFinish(userId);
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStatus() {
        @NotNull final Collection<Project> collection = projectRepository.findAllSortedByStatus();
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntitiesSortedByStatus(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Collection<Project> collection = projectRepository.findAllByUserIdSortedByStatus(userId);
        return collection;
    }

    @NotNull
    @Override
    public Collection<Project> getAllEntities(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return Collections.EMPTY_LIST;
        @NotNull final Collection<Project> collection = projectRepository.findAllByUserId(userId);
        return collection;
    }

}