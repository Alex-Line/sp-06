package com.iteco.linealex.api.repository;

import com.iteco.linealex.enumerate.RoleType;
import com.iteco.linealex.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface IRoleRepository extends CrudRepository<Role, String> {

    Role findByRoleType(RoleType roleType);

    Collection<Role> findAllByUserId(String userId);
}