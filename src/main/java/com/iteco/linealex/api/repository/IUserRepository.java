package com.iteco.linealex.api.repository;

import com.iteco.linealex.model.User;
import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository(value = "user_repository")
public interface IUserRepository extends CrudRepository<User, String> {

    User findOneByLogin(@NotNull final String login);

}