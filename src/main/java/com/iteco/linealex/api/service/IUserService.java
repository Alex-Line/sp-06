package com.iteco.linealex.api.service;

import com.iteco.linealex.model.User;
import org.jetbrains.annotations.Nullable;

public interface IUserService extends IService<User> {

    public void updateUserPassword(
            @Nullable final String oldPassword,
            @Nullable final String newPassword,
            @Nullable final User selectedUser
    ) throws Exception;

    @Nullable
    public User getUser(@Nullable final String login);

}