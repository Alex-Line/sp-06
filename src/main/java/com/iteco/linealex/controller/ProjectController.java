package com.iteco.linealex.controller;

import com.iteco.linealex.api.service.IProjectService;
import com.iteco.linealex.api.service.IUserService;
import com.iteco.linealex.enumerate.Status;
import com.iteco.linealex.model.Project;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
public class ProjectController {

    @Autowired
    IProjectService projectService;

    @Autowired
    IUserService userService;

    @RequestMapping("/project/list")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView listProjects(HttpServletRequest request) {
        @NotNull final String userId = (String) request.getSession().getAttribute("userId");
        System.out.println();
        System.out.println(userId);
        System.out.println();
        @NotNull final ModelAndView modelAndView = new ModelAndView("projectlist");
        modelAndView.addObject("projects", projectService.getAllEntities());
        modelAndView.addObject("userId", userId);
        return modelAndView;
    }

    @RequestMapping("/project/view")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView view(@NotNull final String projectId) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("projectview");
        modelAndView.addObject("project", projectService.getEntityById(projectId));
        return modelAndView;
    }

    @RequestMapping("/project/edit")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView edit(@NotNull final String projectId) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("projectedit");
        modelAndView.addObject("project", projectService.getEntityById(projectId));
        return modelAndView;
    }

    @RequestMapping("/project/delete")
    @PreAuthorize("isAuthenticated()")
    public String delete(@ModelAttribute("projectId") String projectId) {
        projectService.removeEntity(projectId);
        return "redirect:/project/list";
    }

    @RequestMapping(value = "/project/create", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated()")
    public ModelAndView create(
            @ModelAttribute("name") String name,
            @ModelAttribute("description") String description,
            @ModelAttribute("dateStart") Date dateStart,
            @ModelAttribute("dateFinish") Date dateFinish,
            @ModelAttribute("status") String status,
            HttpServletRequest request
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("projectcreate");
        @NotNull final Project createdProject = new Project();
        createdProject.setName(name);
        createdProject.setDescription(description);
        createdProject.setDateStart(dateStart);
        createdProject.setDateFinish(dateFinish);
        createdProject.setStatus(Status.PLANNED);
        if (!status.equals("")) createdProject.setStatus(Status.valueOf(status));
        String userId = (String) request.getSession().getAttribute("userId");
        System.out.println();
        System.out.println(userId);
        System.out.println();
        createdProject.setUser(userService.getEntityById(userId));
        modelAndView.addObject("project", createdProject);
        List<Status> statuses = new ArrayList<>(Arrays.asList(Status.values()));
        modelAndView.addObject("statuses", statuses);
        return modelAndView;
    }

    @RequestMapping(value = "/project/save", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public String save(@ModelAttribute("project") Project project) {
        @NotNull final Project createdProject = new Project();
        createdProject.setName(project.getName());
        createdProject.setDescription(project.getDescription());
        createdProject.setDateStart(project.getDateStart());
        createdProject.setDateFinish(project.getDateFinish());
        createdProject.setStatus(project.getStatus());
        createdProject.setUser(project.getUser());
        projectService.persist(createdProject);
        return "redirect:/project/list";
    }

    @RequestMapping("/project/select")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView select(@ModelAttribute("projectId") String projectId) {
        @NotNull final ModelAndView modelAndView =
                new ModelAndView("tasklist", "projectId", projectId);
        return modelAndView;
    }

}