package com.iteco.linealex.controller;

import com.iteco.linealex.api.service.IProjectService;
import com.iteco.linealex.api.service.ITaskService;
import com.iteco.linealex.api.service.IUserService;
import com.iteco.linealex.enumerate.Status;
import com.iteco.linealex.model.Task;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Controller
public class TaskController {

    @Autowired
    ITaskService taskService;

    @Autowired
    IProjectService projectService;

    @Autowired
    IUserService userService;

    @RequestMapping("/task/list")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView listTasks() {
        @NotNull final ModelAndView modelAndView = new ModelAndView("tasklist");
        modelAndView.addObject("tasks", taskService.getAllEntities());
        return modelAndView;
    }

    @RequestMapping("/task/view")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView view(@NotNull final String taskId) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("taskview");
        modelAndView.addObject("task", taskService.getEntityById(taskId));
        return modelAndView;
    }

    @RequestMapping("/task/edit")
    @PreAuthorize("isAuthenticated()")
    public ModelAndView edit(@NotNull final String taskId) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("taskedit");
        modelAndView.addObject("task", taskService.getEntityById(taskId));
        return modelAndView;
    }

    @RequestMapping("/task/delete")
    @PreAuthorize("isAuthenticated()")
    public String delete(@ModelAttribute("taskId") String taskId) {
        taskService.removeEntity(taskId);
        return "redirect:/task/list";
    }

    @RequestMapping(value = "/task/create", method = RequestMethod.GET)
    @PreAuthorize("isAuthenticated()")
    public ModelAndView create(
            @ModelAttribute("name") String name,
            @ModelAttribute("description") String description,
            @ModelAttribute("dateStart") Date dateStart,
            @ModelAttribute("dateFinish") Date dateFinish,
            @ModelAttribute("status") String status,
            @ModelAttribute("userLogin") String login,
            @ModelAttribute("projectName") String projectName
    ) {
        @NotNull final ModelAndView modelAndView = new ModelAndView("taskcreate");
        @NotNull final Task createdTask = new Task();
        createdTask.setName(name);
        createdTask.setDescription(description);
        createdTask.setDateStart(dateStart);
        createdTask.setDateFinish(dateFinish);
        createdTask.setUser(userService.getUser(login));
        createdTask.setProject(projectService.getEntityByName(projectName));
        if (!status.equals("")) createdTask.setStatus(Status.valueOf(status));
        modelAndView.addObject("task", createdTask);
        List<Status> statuses = new ArrayList<>(Arrays.asList(Status.values()));
        modelAndView.addObject("statuses", statuses);
        return modelAndView;
    }

    @RequestMapping(value = "/task/save", method = RequestMethod.POST)
    @PreAuthorize("isAuthenticated()")
    public String save(@ModelAttribute("task") Task task) {
        System.out.println("there");
        @NotNull final Task createdTask = new Task();
        createdTask.setName(task.getName());
        createdTask.setDescription(task.getDescription());
        createdTask.setDateStart(task.getDateStart());
        createdTask.setDateFinish(task.getDateFinish());
        createdTask.setStatus(task.getStatus());
        createdTask.setUser(task.getUser());
        taskService.persist(createdTask);
        return "redirect:/task/list";
    }

}