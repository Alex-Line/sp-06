package com.iteco.linealex.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.iteco.linealex.enumerate.Status;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@MappedSuperclass
public class AbstractTMEntity extends AbstractEntity {

    @Basic
    @Nullable
    String name = "unnamed";

    @Basic
    @Nullable
    String description = "";

    @Nullable
    @Basic(optional = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DD'T'hh:mm:ss±hh:mm")
    Date dateStart = new Date();

    @Nullable
    @Basic(optional = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "YYYY-MM-DD'T'hh:mm:ss±hh:mm")
    Date dateFinish = new Date();

    //@NotNull
    @ManyToOne
    @Basic(optional = false)
    User user;

    @Basic
    @NotNull
    @Enumerated(value = EnumType.STRING)
    Status status = Status.PLANNED;

}