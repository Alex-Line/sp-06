package com.iteco.linealex.model;

import com.iteco.linealex.enumerate.RoleType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "roles")
public class Role {

    @Id
    @NotNull
    private String id = UUID.randomUUID().toString();

    @Nullable
    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    @Column
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private RoleType roleType = RoleType.ORDINARY_USER;

    public Role(@NotNull RoleType roleType) {
        this.roleType = roleType;
    }

    @Override
    public String toString() {
        return roleType.name();
    }
}