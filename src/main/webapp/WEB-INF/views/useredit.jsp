<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Task Manager</title>
</head>
<body>
    <div align="center">
        <h2>Edit User</h2>
        <form:form action="/user/save" method="post" modelAttribute="user">
            <table border="0" cellpadding="5">
                <tr>
                    <td>ID: </td>
                    <td>${user.id}
                        <form:hidden path="id"/>
                    </td>
                </tr>
                <tr>
                    <td>Login: </td>
                    <td><form:input path="login" /></td>
                </tr>
                <tr>
                    <td>Password: </td>
                    <td><input type="text" name="hashPassword" /></td>

                </tr>
                <tr>
                    <td>Roles: </td>
                    <td><form:input path="roles" /></td>
                </tr>
                <tr>
                    <td colspan="2"><input type="submit" value="Save"></td>
                </tr>
            </table>
        </form:form>
        <a href="/logout"> Sign out</a>
    </div>
</body>
</html>